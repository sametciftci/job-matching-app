import 'job.dart';

class Review {
  int id = 0;
  int resturantId;
  String resturantPhoto;
  String resturantName;
  String location = "Richmond";
  String resturantType;
  String date = "a week ago";
  double rating;
  String content;

  Review(this.resturantId, this.resturantPhoto, this.resturantName,
      this.resturantType, this.rating, this.content);
}

List<Review> reviews = [
  Review(
    jobs[0].id,
    jobs[0].photo,
    jobs[0].name,
    jobs[0].type,
    5.0,
    "Loved it! The staffs are friendly and they have amazing lunch deals.",
  ),
  Review(
    jobs[0].id,
    jobs[1].photo,
    jobs[1].name,
    jobs[1].type,
    4.9,
    "I come here everyday for my coffee! Love the staffs, they have such sweet hearts ❤️",
  )
];
